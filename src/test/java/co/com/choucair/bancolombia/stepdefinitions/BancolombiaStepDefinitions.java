package co.com.choucair.bancolombia.stepdefinitions;

import co.com.choucair.bancolombia.model.BancolombiaDatos;
import co.com.choucair.bancolombia.questions.Answer;
import co.com.choucair.bancolombia.tasks.*;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.Matchers;

import java.util.List;

public class BancolombiaStepDefinitions {
    @Before
    public void setStage (){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^que grupo(\\d+) quiere verificar el funcionamiento de la opción \"([^\"]*)\"$")
    public void queGrupoQuiereVerificarElFuncionamientoDeLaOpción(int arg1, String arg2){
        OnStage.theActorCalled("Grupo2").wasAbleTo(OpenUp.thePage(), Continue.go(), Accept.option());
    }

    @When("^el diligencie los campos del simulador$")
    public void elDiligencieLosCamposDelSimulador(List<BancolombiaDatos> BancolombiaDatos){
        OnStage.theActorInTheSpotlight().attemptsTo(Simulate.data(BancolombiaDatos),Unfold.panel());
    }

    @Then("^el deberia ver en la pantalla la respuesta de su solicitud$")
    public void elDeberiaVerEnLaPantallaLaRespuestaDeSuSolicitud(List<BancolombiaDatos> BancolombiaDatos){
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(BancolombiaDatos), Matchers.equalTo("$236,641")));
    }
}
