#Autor: Grupo2
@validar
Feature: Bancolombia
  Como un usuario
  Quiero ingresar a Bancolombia
  A verificar el funcionamiento calculando y presentando el resultado de "Cuota Mensual"
  @scenario1
  Scenario: Verificar el funcionamiento del simulador al calcular y presentar el resultado en el campo “Cuota Mensual"
    Given que grupo2 quiere verificar el funcionamiento de la opción "cuota mensual"
    When el diligencie los campos del simulador
      |monto   |meses|
      |10000000|48   |
      |10000000|60   |
    Then el deberia ver en la pantalla la respuesta de su solicitud
    |cuotaMensual|
    |  $236,641  |