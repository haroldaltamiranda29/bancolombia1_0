package co.com.choucair.bancolombia.tasks;

import co.com.choucair.bancolombia.userinterface.BancolombiaGoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class Accept implements Task {
    public static Accept option() {
        return Tasks.instrumented(Accept.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.wasAbleTo(
                Click.on(BancolombiaGoPage.CHECK_YES)
        );
    }
}
