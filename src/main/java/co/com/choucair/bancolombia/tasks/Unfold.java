package co.com.choucair.bancolombia.tasks;


import co.com.choucair.bancolombia.userinterface.BancolombiaData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class Unfold implements Task {
    public static Unfold panel() {
        return Tasks.instrumented(Unfold.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BancolombiaData.SPAN)
        );
        try {
            Thread.sleep (10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
