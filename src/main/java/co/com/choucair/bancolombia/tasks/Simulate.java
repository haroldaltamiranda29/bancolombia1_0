package co.com.choucair.bancolombia.tasks;

import co.com.choucair.bancolombia.model.BancolombiaDatos;
import co.com.choucair.bancolombia.userinterface.BancolombiaData;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;

public class Simulate implements Task {
    List<BancolombiaDatos> BancolombiaDatos;

    public Simulate(List<co.com.choucair.bancolombia.model.BancolombiaDatos> bancolombiaDatos) {
        BancolombiaDatos = bancolombiaDatos;
    }

    public static Simulate data(List<BancolombiaDatos> bancolombiaDatos) {
        return Tasks.instrumented(Simulate.class,bancolombiaDatos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(BancolombiaDatos.get(0).getMonto()).into(BancolombiaData.INPUT_AMOUNT),
                Enter.theValue(BancolombiaDatos.get(1).getMeses()).into(BancolombiaData.INPUT_MOTHS),
                Click.on(BancolombiaData.INPUT_DATE),
                Click.on(BancolombiaData.INPUT_YEAR),
                Click.on(BancolombiaData.INPUT_MONTH),
                Click.on(BancolombiaData.INPUT_DAY),
                Click.on(BancolombiaData.BUTTON_SIMULATE)
        );
        try {
            Thread.sleep (10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
