package co.com.choucair.bancolombia.tasks;

import co.com.choucair.bancolombia.userinterface.BancolombiaGoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class Continue implements Task {
    public static Continue go() {
        return Tasks.instrumented(Continue.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BancolombiaGoPage.BUTTON_CONTINUE)
        );
    }
}
