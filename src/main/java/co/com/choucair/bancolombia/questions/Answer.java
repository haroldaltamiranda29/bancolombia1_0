package co.com.choucair.bancolombia.questions;

import co.com.choucair.bancolombia.model.BancolombiaDatos;
import co.com.choucair.bancolombia.userinterface.BancolombiaConfirm;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

public class Answer implements Question<String> {

    List<BancolombiaDatos> bancolombiaDatos;

    public Answer(List<BancolombiaDatos> bancolombiaDatos) {
        this.bancolombiaDatos = bancolombiaDatos;
    }

    public static Answer toThe(List<BancolombiaDatos> bancolombiaDatos) {
        return new Answer(bancolombiaDatos);
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(BancolombiaConfirm.TASA_VARIABLE).viewedBy(actor).asString();
    }
}
