package co.com.choucair.bancolombia.model;

public class BancolombiaDatos {
    private String monto;
    private String meses;
    private String cuotaMensual;

    public String getTasaVariable() {
        return cuotaMensual;
    }

    public void setTasaVariable(String tasaVariable) {
        this.cuotaMensual = tasaVariable;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getMeses() {
        return meses;
    }

    public void setMeses(String meses) {
        this.meses = meses;
    }

}
