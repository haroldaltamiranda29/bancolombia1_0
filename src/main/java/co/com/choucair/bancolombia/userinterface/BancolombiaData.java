package co.com.choucair.bancolombia.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.apache.tools.ant.taskdefs.Tar;
import org.openqa.selenium.By;

public class BancolombiaData extends PageObject {
    public static final Target INPUT_AMOUNT = Target.the("Escribe el monto")
            .located(By.xpath("/html/body/div[2]/div[2]/div/div[2]/div/div[2]/section/div[3]/div[2]/app-root/div/app-solicitud-monto/section/form/mat-form-field[1]/div/div[1]/div[2]/input"));
    public static final Target INPUT_MOTHS = Target.the("Escribe los meses")
            .located(By.xpath("/html/body/div[2]/div[2]/div/div[2]/div/div[2]/section/div[3]/div[2]/app-root/div/app-solicitud-monto/section/form/mat-form-field[2]/div/div[1]/div/input"));
    public static final Target INPUT_DATE = Target.the("Selecciona fecha de nacimiento")
            .located(By.xpath("/html/body/div[2]/div[2]/div/div[2]/div/div[2]/section/div[3]/div[2]/app-root/div/app-solicitud-monto/section/form/mat-form-field[3]/div/div[1]/div/input"));
    public static final Target INPUT_DAY = Target.the("Selecciona el dia")
            .located(By.xpath("//*[@id=\"mat-datepicker-0\"]/div/mat-month-view/table/tbody/tr[3]/td[7]/div"));
    public static final Target INPUT_MONTH = Target.the("Selecciona el mes")
            .located(By.xpath("//*[@id=\"mat-datepicker-0\"]/div/mat-year-view/table/tbody/tr[4]/td[2]/div"));
    public static final Target INPUT_YEAR = Target.the("Selecciona el año")
            .located(By.xpath("//*[@id=\"mat-datepicker-0\"]/div/mat-multi-year-view/table/tbody/tr[4]/td[3]/div"));
    public static final Target BUTTON_SIMULATE = Target.the("Da click en el boton Simular")
            .located(By.xpath("/html/body/div[2]/div[2]/div/div[2]/div/div[2]/section/div[3]/div[2]/app-root/div/app-solicitud-monto/section/form/button"));
    public static final Target SPAN = Target.the("Click en el panel")
            .located(By.xpath("//*[@id=\"mat-expansion-panel-header-0\"]/span[2]"));
}
