package co.com.choucair.bancolombia.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class BancolombiaConfirm {
    public static final Target TASA_VARIABLE = Target.the("")
            .located(By.xpath("/html/body/div[2]/div[2]/div/div[2]/div/div[2]/section/div[3]/div[2]/app-root/div/app-resultado-simulacion/section[1]/swiper/div/div[1]/div[2]/div/div[3]/h4/h4"));
}
