package co.com.choucair.bancolombia.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class BancolombiaGoPage extends PageObject {
    public static final Target BUTTON_CONTINUE = Target.the("Selecciona el boton Continuar")
            .located(By.id("boton-seleccion-tarjeta"));
    public static final Target CHECK_YES = Target.the("Selcecciona el Radio button Si")
            .located(By.cssSelector("#opcion-si > label > div.mat-radio-container"));
}